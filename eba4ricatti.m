function [Z, Y, V, iter, rk, r_res] = eba4ricatti(A,B,C,K,tol,m_max,kp,dtol,M,ip)
%
%  [Z, Y, V, iter, rk, r_res] = eba4ricatti(A,B,C,K,tol,m_max,kp,dtol,M,ip)
%  allows to solve :
%
%  * the continuous Riccati equation:
%
%           A'*X + X*A -X*B*B'*X + C'*C = 0,
%
%  * the dicrete Riccati equation:
%        
%    A'*X*A - (A'*X*B *(B'*X*B + I)^(-1))* B'*X*A + C'*C = X,
%
%  by using the extended block Arnoldi process.
%
%  Input :   
%          A      : Square matrix of size n.
%          B      : Rectangular matrix of size n times p.
%          C      : Rectangular matrix of size s times n.
%          K      : Character string specifying which kind of equations
%                   are solved. If K = 'cont', the continuous Riccati 
%                   equation is solved, while If K = 'disc', the discrete  
%                   Riccati equation is solved, (default K='cont').
%          tol    : Prescripted tolerance, (default tol 1.e-7).  
%                   The computations are stopped when  
%                   norm(R(X))/norm(C*C') < tol and where R(X) is the    
%                   residual associated to X, i.e., 
%                   R(X) = A'*X+X*A-X*B*B'*X+C'*C in the contiluous case,
%                   R(X) = A'*X*A - (A'*X*B *(B'*X*B + I)^(-1))* B'*X*A 
%                          + C'*C) - X in the discrete case. 
%          m_max  : Maximum number of iterations, (default m_max = 50).                      
%          kp     : To save computations, the projected Riccati equations
%                   are formed and solved every kp iterations, 
%                   (default kp = 1).
%          dtol   : Tolerance used to discard columns in the singular 
%                   value decomposition of Y the solution of the projected
%                   Riccati equation, (default dtol = 1.e-12).               
%          M      : Character string specifying the method used to solve 
%                   the linear systems involved in the matrix-vector 
%                   products with the inverse of A.  When M = 'LU', the  
%                   linear systems are solved with the help of LU 
%                   factorization, while if M='GMRES', the iterative GMRES  
%                   method is used, (default M = 'LU').
%          ip     : If ip = 1, the iteration number, the dimension of the
%                   constructed Krylov subspace, the relative residual 
%                   norm and the rank of X are displayed in the command 
%                   window. If ip = 0, and the above parameters are not 
%                   displayed in the command window, (default ip = 0).
%             
%  Output :     
%           Z     : The singular value decomposition part of the
%                   approximate solution X, i.e. X = Z*Z'. Note that the
%                   size of Z is n times l, where l depends on dtol. 
%           Y     : Square matrix which is the solution of the projected 
%                   Riccati equation. The size of Y is 2*s*iter.
%           V     : Rectangular matrix which is the Krylov basis
%                   constructed by the extended block Arnoldi process.
%                   The size of V is n \times 2*s*iter.
%           iter  : Final iteration number, i.e., integer indicating the 
%                   iterations number needed to achieve convergence.
%           rk    : Integer specifying the rank of the approximate 
%                   solution X.
%           r_res : Relatif residual norm of the computed approximate
%                   solution X, i.e., r_res = norm(R(X))/norm(C*C').
% 
% Notice that the computed approximate solution X is given either by :  
% X = Z*Z' or by X = V*Y*V'.
%
% For more details on this method, see the paper :
%          "AN EXTENDED BLOCK ARNOLDI ALGORITHM FOR LARGE-SCALE
%       SOLUTIONS OF THE CONTINUOUS-TIME ALGEBRAIC RICCATI EQUATION"
% appeared in Electronic Transactions on Numerical Analysis,
% Volume 33, pp. 53-62, 2009.
%     
%
% Authors          : M. HEYOUNI, N. TAIBI and K. JBILOU.
% Last modified    : 30/09/2015.
%
if (nargin < 10)
    ip = 0; 
end
if (nargin >= 9 && strcmp(M,'GMRES') == 1 )
    rest_gmres = 50; tol_gmres = 1.e-9; maxit_gmres = 50; 
end
if (nargin < 9)
    M ='LU'; 
end
if (nargin < 8)
    dtol = 1.e-12;
end
if (nargin < 7)
    kp = 1;
end
if (nargin < 6)
    m_max = 50;
end
if (nargin < 5)
    tol = 1.e-7;
end
if (nargin < 4)
    K = 'cont';
end

[s, n] = size(C); p = size(B,2); s2 = 2*s;
H = sparse((m_max+1)*s2,m_max*s2); t = sparse((m_max+1)*s2,m_max);
T = sparse((m_max+1)*s2,m_max*s2); odds = []; Y=[]; Up = zeros(n,s2);
At = A'; c = C'; res_c = norm(C)^2; 

if (strcmp(M,'LU') == 1) 
   [LA,UA] = lu(At); 
   c1 = UA \ (LA \ c); 
end
if (strcmp(M,'GMRES') == 1) 
   [LL,UU] = ilu(At);
   for i = 1:s
     [c1(:,i),~] = gmres(At,c(:,i),rest_gmres,tol_gmres,maxit_gmres,LL,UU); 
   end 
end

[U(:,1:s2), beta] = qr([c, c1],0); ibeta = inv(beta); 
beta = beta(1:s,1:s); beta2 = beta*beta'; Bj = U(:,1:s2)'*B;

for j = 1:m_max % j-th iteration of the extended block Arnoldi process
    
    jms = (j-1)*s2+1; j1s = (j+1)*s2; js = j*s2; 
    js1 = js+1; jsh = (j-1)*s2+s;
    % matrix-vector product with A'.
    Up(:,1:s) = At*U(:,jms:jsh); 
    % matrix-vector product with invA (inverse of A')
    if (strcmp(M,'LU') == 1)                     % The LU decompostion  
        Up(:,s+1:s2) = UA \ (LA \ U(:,jsh+1:js));% is used to solve linear
    end                                          % systems with A.
    if (strcmp(M,'GMRES') == 1) 
       for i = 1:s
           [Up(:,s+i),~] = gmres(At,U(:,jsh+i),...  % The GMRES method is 
           rest_gmres,tol_gmres,maxit_gmres,LL,UU); % used to solve linear
       end                                          % systems with A.
    end
    
    for il = 1:2 
        k_min = max(1,j-m_max);
        for kk = k_min:j
            k1 = (kk-1)*s2+1; k2 = kk*s2;
            coef = U(1:n,k1:k2)'*Up;
            H(k1:k2,jms:js) = H(k1:k2,jms:js)+ coef;
            Up = Up - U(:,k1:k2)*coef;
        end
    end
    if ( j <= m_max )
       [U(1:n,js1:j1s),h] = qr(Up,0); 
       H(js1:j1s,jms:js) = h; hinv = inv(h);
    end
    
    Bj = [Bj ; U(1:n,js1:j1s)'*B]; 
    
    Id = speye(js+s2);
    
    if ( j == 1 ) % Computing the matrix Tm = V_m'*A*V_m
       t(1:js+s,(j-1)*s+1:j*s) = [H(1:s2+s,1:s)/ibeta(1:s,1:s), ...
           speye(s2+s,s)/ibeta(1:s,1:s)]*ibeta(1:s2,s+1:s2); 
    else
       t(1:js+s2,(j-1)*s+1:j*s) = ...
          t(1:js+s2,(j-1)*s+1:j*s) + H(1:js+s2,jms:jms-1+s)*rho; 
    end
    
    odds = [odds, jms:(jms-1+s)];        
    evens = 1:js; evens(odds) = [];       
    T(1:js+s2,odds) = H(1:js+s2,odds);      
    T(1:js+s,evens) = t(1:js+s,1:j*s); 
    t(1:js+s2,j*s+1:(j+1)*s) = (Id(1:js+s2,(js-s+1):js)- ...
        T(1:js+s2,1:js)*H(1:js,js-s+1:js))*hinv(s+1:s2,s+1:s2); 
    
    rho = hinv(1:s,1:s)\hinv(1:s,s+1:s2); % End comp. Tm = V_m'*A*V_m
    
    if ( rem(j,kp) == 0 ) % The projected Riccati equations are
                          % solved every kp iterations
        
       Btilde = Bj(1:js,:); 
       rhs = full(speye(js,s)*beta2*speye(js,s)');
       
       if (strcmp(K,'cont') == 1)          
          Y = care(full(T(1:js,1:js)'),Btilde,rhs);
          nr = norm(T(js+1:js+s,jms:js)*Y(jms:js,:)); 
       end
       
       if (strcmp(K,'disc') == 1)       
          Y = dare(full(T(1:js,1:js)'),Btilde,rhs);
          Gj = T(js+1:js+s2,jms:js)*Y(jms:js,:);
          Jj = Gj*Btilde*((eye(p)+Btilde'*Y*Btilde)^(-1))*Btilde'*Y;
          est = 2*norm((Gj-Jj)*T(1:js,1:js)','fro')^2 + ...
          norm((Gj(:,jms:js)-Jj(:,jms:js))*T(js+1:js+s2,jms:js)','fro')^2;
          nr = sqrt(est);
       end 
       
       r_res = nr/res_c;
       
       if (ip == 1)
           fprintf('%3.0f %3.0f %3.2e\n',j,js,nr/res_c); 
       end
                
       if ( r_res < tol )            %   
          iter = j; V = U(:,1:js);                    % Using the SVD of Y, 
          [uY,sY] = eig(Y); [sY,id] = sort(diag(sY)); % we give the
          sY = flipud(sY); uY = uY(:,id(end:-1:1));   % solution X as a 
          is = sum(abs(sY) > dtol);                   % a product of two
          Y0 = uY(:,1:is)*diag(sqrt(sY(1:is)));       % low rank matrices
          Z = U(:,1:js)*Y0;                           % i,e., X = Z*Z'
          rk = rank(Z);
          if (ip == 1)
             fprintf('\n') 
             fprintf('Iteration number, dimension of the Krylov ');
             fprintf('subspace and rank \nof the solution X are ');
             fprintf('respectively: %4.0f %4.0f %4.0f\n',j,js,rk);
             fprintf('Relative residual norm is %3.2e\n',nr/res_c);
          end
          break,    
       end      
       if ( j == m_max && nr/res_c >= tol )
          Z=[]; Y=[]; V=[]; iter=0; rk=[];
          disp('no convergence');
       end

    end

end