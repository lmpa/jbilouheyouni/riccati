  The software contained in this library corresponds to the extended block Arnoldi algorithm for the computation of approximate solutions to  large-scale algebraic Riccati equations. The method is a projection method onto an extended block Krylov subspace defined as the sum of two block Krylov subspaces associated with a nonsingular matrix $A$ and its inverse $A^{-1}$.We also give the Matlab software corresponding to the method with some numerical tests.
  
  
   THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED WARRANTY.                                                           ANY USE OF THE SOFTWARE CONSTITUTES  ACCEPTANCE OF THE TERMS OF THE ABOVE STATEMENT.

# Authors 
                                           
     M. HEYOUNI                            
     Université du Littoral, Calais, France
     E-mail: mohammed.heyouni@univ-littoral.fr
     
     K. JBILOU                                                            
     Université du Littoral, Calais, France                               
     E-mail: khalide.jbilou@univ-littoral.fr                                


# References
                                                                         
   "AN EXTENDED BLOCK ARNOLDI ALGORITHM FOR LARGE-SCALE                  
    SOLUTIONS OF THE CONTINUOUS-TIME ALGEBRAIC RICCATI EQUATION"         
    Electronic Transactions on Numerical Analysis,                       
    Volume 33, pp. 53-62, 2009.                                          
                                                                         
   "BLOCK ARNOLDI-BASED METHODS FOR LARGE SCALE DISCRETE-TIME            
    ALGEBRAIC RICCATI EQUATIONS"                                         
    Journal of Computational and Applied Mathematics,                    
    Volume 236 (6), pp. 1531-1542, 2011.                                 

# Software description

   SOFTWARE REVISION:

       Ver 1.0  September 2015

   SOFTWARE LANGUAGE:

       MATLAB R2012b (800.783)

   
   ## SOFTWARE
  
   
   This software provides a Matlab source code  to solve the continuous  
   Riccati equation: A'*X + X*A -X*B*B'*X + C'*C = 0, as well as the discrete   
   Algebraic Riccati Equation   
   A'*X*A - (A'*X*B *(B'*X*B + I)^(-1))* B'*X*A + C'*C = X by using the 
   extended block Arnoldi process. The toolbox also contains auxiliary 
   scripts to test the implemented algorithm's performances.


  ##  PACKAGE


   This software package is shipped as a zipped archive, named 
   ExtBlAr4Ric.zip. After unzipping, it has the following structure:
   
   ExtBlAr4Ric.zip./
                   MARKET/
                   IMTEK1/
                   IMTEK2/
                   results/
                   readme.txt
                   eba4ricatti.m
                   mmread.m
                   ContRiccati.m
                   DiscRiccati.m
                   fdm_2d_matrix.m
   
   The main directory (ExtBlAr4Ric) contains the following files:
   
   readme.txt   : This file.
   
   mmread.m 	: Utility code to load the contents of the Matrix 
                  Market stored in file into the matrix A. 
                  The matrices examples are stored in IMTEK1, IMTEK2 
                  and MARKET directories. The mmread.m file comes
                  from the Matrix Market web repository [NIST 2007].
   
   eba4ricatti.m : Code implementing the extended block Arnoldi
                   algorithm for both cases : the continuous Riccati equation: 
                   A'*X + X*A -X*B*B'*X + C'*C = 0 (EBA-CARE)
                   and discrete Riccati equation  (EBA-DARE) : 
	           A'*X*A - (A'*X*B *(B'*X*B + I)^(-1))* B'*X*A + C'*C = X
                         
   ContRiccati.m : Script to test/validate our implementation on various
                   matrices examples. No input parameters are needed, the
    	             script is menu based. This script address the continuous  
                   Riccati equations case by using the extended block
                   Arnoldi process (EBA-CARE)
                              
   DiscRiccati.m : Similar to the above script but intended for 
	           solving discrete Riccati equations.
   
   fdm_2d_matrix.m : Matlab code to generate the stiffness matrix A for
	  	     the finite difference discretization of the PDE :
                     L(u)=Laplacien(u)-f(x,y)(du/dx)-g(x,y)(du/dy)-h(x,y)u
                     on the unit square [0,1]x[0,1] with homogeneous 
                     Dirichlet boundary conditions. This file comes 
	  	     from the LYAPACK toolbox [Penzl 2000].
   
   IMTEK1/IMTEK2/MARKET	: Folders holding different kinds of matrices for
                          test purposes.
    
   results/	: Folder where the results/traces of a different tests 
                  are stored.
   
  
## How to install
  

   Installing the package implementing the extended block Arnoldi algorithm  
   is very simple:
   
   Step 1. Unzip the file ExtBlAr4Ric.zip, the "ExtBlAr4Ric" directory will
           be created.
   
   Step 2. Start MATLAB
   
   Step 3. Add the root directory ExtBlAr4Ric/ to the Matlab path via the
           MATLAB command 'addpath' to your search directory to use the 
	   functions provided by the ExtBlAr4Ric package, e.g., 
	   'addpath installationDirectory/ExtBlAr4Ric/' or Change to root   
           folder of the unzipped ExtBlAr4Ric.zip.
   
   Step 4. Remove the compressed package file
   

## How to test
     
   You may test the installation by running the provided test scripts
   ContRiccati.m (or DiscRiccati.m)  from the root folder as follow :

   (Matlab prompt)> ContRiccati or 
   (Matlab prompt)> DiscRiccati

   A menu, will be displayed to choose the example to run.
   The m-file (ContRiccati.m or DiscRiccati.m), then, loads the chosen Matrix 
   from the IMTEK1/IMTEK2 or MARKET folder and solves the continuous and/or 
   discrete Riccati equation by the newly implemented algorithm.

   The input parameters, as well as the involved matrices, A, B and C, can be
   changed directly in the current script's source code (ContRiccati.m or 
   DiscRiccati.m) to handle new examples.
   
   Finally the "results" folder will track all the results as they are shown  
   in the Matlab window when running the test scripts.

## Documentation
   
   More information can be found in the papers referenced above and the 
   code itself.





