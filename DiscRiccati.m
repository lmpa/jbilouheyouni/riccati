%
% Matlab script to test EBA-DARE "Extended Block Arnoldi for the discrete  
% Algebraic Riccati Equation". This algorithm is intended to solve the 
% discrete Riccati equation: 
%
%       A'*X*A - (A'*X*B *(B'*X*B + I)^(-1))* B'*X*A + C'*C = X,
% 
% where A is an n_x_n matrix, B  is an n_x_p matrix and C is an s_x_n 
% matrix. The matrix test A can be:
%
% * chosen from IMTEK which is a benchmark problem coming from a  
%   discretization of a convective thermal flow. (choice = 'IMTEK1'  
%   or 'IMTEK2'). In this case the matrices B, C are also given.
%   For more details, see the reference:
%
%   C. Moosmann, E. B. Rudnyi, A. Greiner and J. G. Korvink, 
%     "Model Order Reduction for Linear Convective Thermal Flow", 
%   Proceedings of 10th International Workshops on THERMal INvestigations 
%   of ICs and Systems, THERMINIC2004, 29 Sept - 1 Oct, 2004,
%   Sophia Antipolis, France.
%
% * generated from the centered finite difference discretization of
%   the operator :
%
%   laplace(u) - fx du/dx - fy du/dy - g u   =   r.h.s.     on Omega
%                 
%                                        u   =   0          on dOmega
% 
%  Omega = (0,1)x(0,1)   (unit square). 
%
%  For more details, see the MATLAB function fdm_2d_matrix from the LYAPACK 
%  library and the reference:
%
%     "LYAPACK A MATLAB toolbox for Large Lyapunov and Riccati Equations, 
%  Model Reduction Problems, and Linear-quadratic Optimal Control Problems", 
%  http://www.tu-chemintz.de/sfb393/lyapack.
%
% * taken from the Matrix-Market or from the university of Florida sparse 
%   matrix collection. See : 
%   - Matrix Market ---> http://math.nist.gov/MatrixMarket/
%   - UF sparse matrix ---> https://www.cise.ufl.edu/research/sparse/matrices/
%
% In this two last cases, the matrices B and C are generated randomly.
%
% For an algorithmic description and more mathematical details about the
% EBA-ARE "EXTENDED BLOCK ARNOLDI for solving ALGEBRAIC RICCATI EQUATION"
% we refer to the papers :
%
% * A. Bouhamidi, M. Heyouni and K. Jbilou.
%          "BLOCK ARNOLDI-BASED METHODS FOR LARGE-SCALE
%           DISCRETE-TIME ALGEBRAIC RICCATI EQUATIONS"
%   appeared in Journal of Computational and Applied Mathematics,
%   Volume 236, pp. 1531�1542, 2011. 
%

clear all; close all; format short e; 

fprintf('\n')
disp('**************************************************');
disp('* Extended Block Arnoldi for DARE testing script *');
disp('**************************************************');
fprintf('\n')
disp('           1- Use matrices in IMTEK1 folder.'); 
fprintf('\n')
disp('           2- Use matrices in IMTEK2 folder.');
fprintf('\n')
disp('           3- Use FDM2D matrix.');
fprintf('\n')
disp('           4- Use matrices in MARKET folder.');
fprintf('\n')
disp('           0- Quit.');
fprintf('\n');

prompt = 'Please enter your choice : ';

choice = input(prompt);

if(choice == 0) 

    fprintf('\n')
    disp('Have a nice day ! bye');
    fprintf('\n')
    return;

elseif (choice == 1) % choice = IMTEK1

    %   First IMTEK Example :
    %   *********************

    fprintf('\n')
    disp('Example 1 : IMTEK1 Example.');
    fprintf('\n')
    A = mmread('IMTEK1/flow_meter_model_v0.5.A.mtx'); n = length(A);
    B = mmread('IMTEK1/flow_meter_model_v0.5.B.mtx'); p = size(B,2); 
    C = mmread('IMTEK1/flow_meter_model_v0.5.C.mtx'); s = size(C,1); 
    E = mmread('IMTEK1/flow_meter_model_v0.5.E.mtx'); 
    C = full(C); B = full(B); A = E\A; B = E\B;
 
elseif (choice == 2) % choice = IMTEK2

    %   Second IMTEK Example :
    %   **********************

    fprintf('\n')
    disp('Example 2 : IMTEK2 Example.');
    fprintf('\n')
    A = mmread('IMTEK2/flow_meter_model_v0.A.mtx'); n = length(A);
    B = mmread('IMTEK2/flow_meter_model_v0.B.mtx'); p = size(B,2); 
    C = mmread('IMTEK2/flow_meter_model_v0.C.mtx'); s = size(C,1); 
    E = mmread('IMTEK2/flow_meter_model_v0.E.mtx'); 
    C = full(C); B = full(B); A = E\A; B = E\B;

else
    
    fprintf('\n')
    disp('The coefficient matrices B and C are:'); 
    fprintf('\n')
    disp('      0- for B = rand(n,p) and C = rand(s,n).');
    fprintf('\n')
    disp('      1- for B = [I_p; 0_(n-p+1)_x_n] and C = [O_s_x_(n-s),I_s]');
    fprintf('\n')
    rand_id = 'Please enter your choice :  ';
    rand_id = input(rand_id);
    
    if (choice == 3) %

       %   PDE Example :
       %   *************

       fprintf('\n')
       disp('Example 3  : PDE Example.');
       fprintf('\n')
       prompt = 'Please enter n0 the number of inner grids for PDE disc. ';
       n0 = input(prompt);
       A = fdm_2d_matrix(n0,'x+10*y^2','sqrt(2*x^2+y^2)','x^2-y^2');
       n = length(A);   
    
    elseif (choice == 4) %  

       %   MATRIX MARKET Example : the Default choice.
       %   ******************************************

       fprintf('\n')
       disp('Example 4  : MATRIX MARKET Example.');
       fprintf('\n')
       A = -mmread('MARKET/epb1.mtx'); 
       %A = -mmread('MARKET/psmigr_3.mtx');
       %A = -mmread('MARKET/cavity17.mtx');
       %A = -mmread('MARKET/cage9.mtx'); 
       n = length(A);
    
   end

end

if (choice == 3 || choice == 4)
    fprintf('\n')
    prompt = 'Enter p the rank of B '; p = input(prompt);
    fprintf('\n')
    prompt = 'Enter s the rank of C '; s = input(prompt);
    fprintf('\n')
    if (rand_id == 0)
        B = rand(n,p); C = rand(s,n);
    end
    if (rand_id == 1)
        B = zeros(n,p); B(1:p,1:p) = eye(p);
        C = zeros(s,n); C(1:s,n-s+1:end) = eye(s);
    end
end

fprintf('The size of the coefficient matrices are: \n');
fprintf('A = %d x %d\n',size(A,1),size(A,2));
fprintf('B = %d x %d\n',size(B,1),size(B,2));
fprintf('C = %d x %d\n',size(C,1),size(C,2));
fprintf('\n');

m_max = 50; tol = 1.e-7; k_max = m_max; 
kp = 5; dtol = 1.e-12; s_max = 2000;

A = A/norm(A,1); % we normalize for the discrete case

disp('3. Discrete case: comput. of A^(-1)*V via LU factorization');
disp('==========================================================');

t0 = cputime;
 [Z3,Y3,V3,it3,rk3,rr3] = eba4ricatti(A,B,C,'disc',tol,m_max,kp,dtol,'LU',0);
t3 = cputime-t0;

if (it3 > 0)
    fprintf('\n')
    fprintf('Iteration number is                 %8.0f\n',it3);
    fprintf('Dimension of the Krylov subspace is %8.0f\n',it3*2*s);
    fprintf('Rank of X is                        %8.0f\n',rk3);
    fprintf('Computed relatif residual norm  is  %3.2e\n',rr3);
    fprintf('CPU time is                         %3.2e\n',t3);
    fprintf('\n')
    if (n < s_max)
        X3 = V3*Y3*V3'; X33 = Z3*Z3'; 
        res3 = norm(X3-(A'*X3*A- ...
              (A'*X3*B*(B'*X3*B + eye(p))^(-1))*B'*X3*A + C'*C),'fro'); 
        res33 = norm(X33-(A'*X33*A- ...
              (A'*X33*B*(B'*X33*B + eye(p))^(-1))*B'*X33*A + C'*C),'fro');
        fprintf('Exact residual norm with X = Z*Z^T is     %3.2e\n',res3);
        fprintf('Exact residual norm with X = V*Y*V^T is   %3.2e\n',res33);
    end 
    fprintf('\n')
end

disp('4. Discrete case: comput. of A^(-1)*V via GMRES solver');
disp('======================================================');

t0 = cputime;
 [Z4,Y4,V4,it4,rk4,rr4] = eba4ricatti(A,B,C,'disc',tol,m_max,kp,dtol,'GMRES',0);
t4 = cputime-t0;

if (it4 > 0)
    fprintf('\n')
    fprintf('Iteration number is                 %8.0f\n',it4);
    fprintf('Dimension of the Krylov subspace is %8.0f\n',it4*2*s);
    fprintf('Rank of X is                        %8.0f\n',rk4);
    fprintf('Computed relatif residual norm  is  %3.2e\n',rr4);
    fprintf('CPU time is                         %3.2e\n',t4);
    fprintf('\n')
    if (n < s_max)
        X4 = V4*Y4*V4'; X44 = Z4*Z4'; 
        res4 = norm(X4-(A'*X4*A- ...
              (A'*X4*B*(B'*X4*B + eye(p))^(-1))*B'*X4*A + C'*C),'fro'); 
        res44 = norm(X44-(A'*X44*A- ...
              (A'*X44*B*(B'*X44*B + eye(p))^(-1))*B'*X44*A + C'*C),'fro');
        fprintf('Exact residual norm with X = Z*Z^T is     %3.2e\n',res4);
        fprintf('Exact residual norm with X = V*Y*V^T is   %3.2e\n',res44);
    end
    %fprintf('\n')
end