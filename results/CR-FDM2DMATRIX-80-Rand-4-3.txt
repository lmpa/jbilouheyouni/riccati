>> ContRiccati

*************************************************
* Extended Block Arnoldi for ARE testing script *
*************************************************

           1- Use matrices in IMTEK1 folder.

           2- Use matrices in IMTEK2 folder.

           3- Use FDM2D matrix.

           4- Use MARKET matrix in MARKET folder.

           0- Quit.

Please enter your choice : 3

The coefficient matrices B and C are:

      0- for B = rand(n,p) and C = rand(s,n).

      1- for B = [I_p; 0_(n-p+1)_x_n] and C = [O_s_x_(n-s),I_s]

Please enter your choice :  0

Example 3  : PDE Example.

Please enter n0 the number of inner grids for PDE disc. 80

Enter p the rank of B 4

Enter s the rank of C 3

The size of the coefficient matrices are: 
A = 6400 x 6400
B = 6400 x 4
C = 3 x 6400

1. Continuous case: comput. of A^(-1)*V  via LU factorization
=============================================================

Iteration number is                       20
Dimension of the Krylov subspace is      120
Rank of X is                              59
Computed relatif residual norm  is  5.53e-10
CPU time is                         2.27e+00


2. Continuous case: comput. of A^(-1)*V via GMRES solver
========================================================

Iteration number is                       20
Dimension of the Krylov subspace is      120
Rank of X is                              59
Computed relatif residual norm  is  5.53e-10
CPU time is                         4.58e+01