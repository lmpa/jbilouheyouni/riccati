>> ContRiccati

*************************************************
* Extended Block Arnoldi for ARE testing script *
*************************************************

           1- Use matrices in IMTEK1 folder.

           2- Use matrices in IMTEK2 folder.

           3- Use FDM2D matrix.

           4- Use MARKET matrix in MARKET folder.

           0- Quit.

Please enter your choice : 3

The coefficient matrices B and C are:

      0- for B = rand(n,p) and C = rand(s,n).

      1- for B = [I_p; 0_(n-p+1)_x_n] and C = [O_s_x_(n-s),I_s]

Please enter your choice :  1

Example 3  : PDE Example.

Please enter n0 the number of inner grids for PDE disc. 80

Enter p the rank of B 4

Enter s the rank of C 5

The size of the coefficient matrices are: 
A = 6400 x 6400
B = 6400 x 4
C = 5 x 6400

1. Continuous case: comput. of A^(-1)*V via LU factorization
============================================================

Iteration number is                       15
Dimension of the Krylov subspace is      150
Rank of X is                              42
Computed relatif residual norm  is  8.40e-08
CPU time is                         2.67e+00


2. Continuous case: comput. of A^(-1)*V via GMRES solver
========================================================

Iteration number is                       15
Dimension of the Krylov subspace is      150
Rank of X is                              42
Computed relatif residual norm  is  4.73e-08
CPU time is   