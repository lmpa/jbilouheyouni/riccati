%
% Matlab script to test EBA-CARE "Extended Block Arnoldi for the continuous  
% Algebraic Riccati Equation". This algorithm is intended to solve the 
% continuous Riccati equation
%
%       A'*X + X*A - X*B*B'*X + C'*C = 0,
% 
% where A is an n_x_n matrix, B  is an n_x_p matrix and C is an s_x_n 
% matrix. The matrix test A can be:
%
% * chosen from IMTEK which is a benchmark problem coming from a  
%   discretization of a convective thermal flow. (choice = 'IMTEK1'  
%   or 'IMTEK2'). In this case the matrices B, C are also given.
%   For more details, see the reference:
%
%   C. Moosmann, E. B. Rudnyi, A. Greiner and J. G. Korvink, 
%     "Model Order Reduction for Linear Convective Thermal Flow", 
%   Proceedings of 10th International Workshops on THERMal INvestigations 
%   of ICs and Systems, THERMINIC2004, 29 Sept - 1 Oct, 2004,
%   Sophia Antipolis, France.
%
% * generated from the centered finite difference discretization of
%   the operator :
%
%   laplace(u) - fx du/dx - fy du/dy - g u   =   r.h.s.     on Omega
%                 
%                                        u   =   0          on dOmega
% 
%  Omega = (0,1)x(0,1)   (unit square). 
%
%  For more details, see the MATLAB function fdm_2d_matrix from the LYAPACK 
%  library and the reference:
%
%     "LYAPACK A MATLAB toolbox for Large Lyapunov and Riccati Equations, 
%  Model Reduction Problems, and Linear-quadratic Optimal Control Problems", 
%  http://www.tu-chemintz.de/sfb393/lyapack.
%
% * taken from the Matrix-Market or from the university of Florida sparse 
%   matrix collection. See : 
%   - Matrix Market ---> http://math.nist.gov/MatrixMarket/
%   - UF sparse matrix ---> https://www.cise.ufl.edu/research/sparse/matrices/
%
% In this two last cases, the matrices B and C are generated randomly.
%
% For an algorithmic description and more mathematical details about the
% EBA-ARE "EXTENDED BLOCK ARNOLDI for solving ALGEBRAIC RICCATI EQUATION"
% we refer to the papers :
%
% * M. Heyouni and K. Jbilou.
%          "AN EXTENDED BLOCK ARNOLDI ALGORITHM FOR LARGE-SCALE
%       SOLUTIONS OF THE CONTINUOUS-TIME ALGEBRAIC RICCATI EQUATION"
%   appeared in Electronic Transactions on Numerical Analysis,
%   Volume 33, pp. 53-62, 2009.
%

clear all; close all; format short e; 

fprintf('\n')
disp('**************************************************');
disp('* Extended Block Arnoldi for CARE testing script *');
disp('**************************************************');
fprintf('\n')
disp('           1- Use matrices in IMTEK1 folder.'); 
fprintf('\n')
disp('           2- Use matrices in IMTEK2 folder.');
fprintf('\n')
disp('           3- Use FDM2D matrix.');
fprintf('\n')
disp('           4- Use matrices in MARKET folder.');
fprintf('\n')
disp('           0- Quit.');
fprintf('\n');

prompt = 'Please enter your choice : ';

choice = input(prompt);

if(choice == 0) 

    fprintf('\n')
    disp('Have a nice day ! bye');
    fprintf('\n')
    return;

elseif (choice == 1) % choice = IMTEK1

    %   First IMTEK Example :
    %   *********************

    fprintf('\n')
    disp('Example 1 : IMTEK1 Example.');
    fprintf('\n')
    A = mmread('IMTEK1/flow_meter_model_v0.5.A.mtx'); n = length(A);
    B = mmread('IMTEK1/flow_meter_model_v0.5.B.mtx'); p = size(B,2); 
    C = mmread('IMTEK1/flow_meter_model_v0.5.C.mtx'); s = size(C,1); 
    E = mmread('IMTEK1/flow_meter_model_v0.5.E.mtx'); 
    C = full(C); B = full(B); A = E\A; B = E\B;
 
elseif (choice == 2) % choice = IMTEK2

    %   Second IMTEK Example :
    %   *********************

    fprintf('\n')
    disp('Example 2 : IMTEK2 Example.');
    fprintf('\n')
    A = mmread('IMTEK2/flow_meter_model_v0.A.mtx'); n = length(A);
    B = mmread('IMTEK2/flow_meter_model_v0.B.mtx'); p = size(B,2); 
    C = mmread('IMTEK2/flow_meter_model_v0.C.mtx'); s = size(C,1); 
    E = mmread('IMTEK2/flow_meter_model_v0.E.mtx'); 
    C = full(C); B = full(B); A = E\A; B = E\B;

else
    
    fprintf('\n')
    disp('The coefficient matrices B and C are:'); 
    fprintf('\n')
    disp('      0- for B = rand(n,p) and C = rand(s,n).');
    fprintf('\n')
    disp('      1- for B = [I_p; 0_(n-p+1)_x_n] and C = [O_s_x_(n-s),I_s]');
    fprintf('\n')
    rand_id = 'Please enter your choice :  ';
    rand_id = input(rand_id);%
    
    if (choice == 3) %

       %   PDE Example :
       %   *************

       fprintf('\n')
       disp('Example 3  : PDE Example.');
       fprintf('\n')
       prompt = 'Please enter n0 the number of inner grids for PDE disc. ';
       n0 = input(prompt);
       A = fdm_2d_matrix(n0,'x+10*y^2','sqrt(2*x^2+y^2)','x^2-y^2');
       n = length(A);   
    
    elseif (choice == 4) %  

       %   MATRIX MARKET Example : the Default choice.
       %   ******************************************

       fprintf('\n')
       disp('Example 4  : MARKET matrix Example.');
       fprintf('\n')
       A = -mmread('MARKET/epb1.mtx'); 
       %A = -mmread('MARKET/psmigr_3.mtx');
       %A = -mmread('MARKET/cavity17.mtx');
       %A = -mmread('MARKET/cage9.mtx'); 
       n = length(A);
    
   end

end

if (choice == 3 || choice == 4)
    fprintf('\n')
    prompt = 'Enter p the rank of B '; p = input(prompt);
    fprintf('\n')
    prompt = 'Enter s the rank of C '; s = input(prompt);
    fprintf('\n')
    if (rand_id == 0)
        B = rand(n,p); C = rand(s,n);
    end
    if (rand_id == 1)
        B = zeros(n,p); B(1:p,1:p) = eye(p);
        C = zeros(s,n); C(1:s,n-s+1:end) = eye(s);
    end
end

fprintf('The size of the coefficient matrices are: \n');
fprintf('A = %d x %d\n',size(A,1),size(A,2));
fprintf('B = %d x %d\n',size(B,1),size(B,2));
fprintf('C = %d x %d\n',size(C,1),size(C,2));
fprintf('\n');

m_max = 50; tol = 1.e-7; k_max = m_max; 
kp = 5; dtol = 1.e-12; s_max = 2000;

disp('1. Continuous case: comput. of A^(-1)*V  via LU factorization');
disp('=============================================================');

t0 = cputime;
 [Z1,Y1,V1,it1,rk1,rr1] = eba4ricatti(A,B,C,'cont',tol,m_max,kp,dtol,'LU',0);
t1 = cputime-t0;

if (it1 > 0)
    fprintf('\n')
    fprintf('Iteration number is                 %8.0f\n',it1);
    fprintf('Dimension of the Krylov subspace is %8.0f\n',it1*2*s);
    fprintf('Rank of X is                        %8.0f\n',rk1);
    fprintf('Computed relative residual norm  is %3.2e\n',rr1);
    fprintf('CPU time is                         %3.2e\n',t1);
    fprintf('\n')
    if (n < s_max)
        X1 = V1*Y1*V1'; X11 = Z1*Z1';  
        res1 = norm(A'*X1+X1*A-X1*(B*B')*X1+C'*C,'fro'); 
        res11 = norm(A'*X11+X11*A-X11*(B*B')*X11+C'*C,'fro');
        fprintf('Exact residual norm with X = Z*Z^T is     %3.2e\n',res1);
        fprintf('Exact residual norm with X = V*Y*V^T is   %3.2e\n',res11);
    end
    fprintf('\n')
end

disp('2. Continuous case: comput. of A^(-1)*V via GMRES solver');
disp('========================================================');

t0 = cputime;
 [Z2,Y2,V2,it2,rk2,rr2] = eba4ricatti(A,B,C,'cont',tol,m_max,kp,dtol,'GMRES',0);
t2 = cputime-t0;

if (it2 > 0)
    fprintf('\n')
    fprintf('Iteration number is                 %8.0f\n',it2);
    fprintf('Dimension of the Krylov subspace is %8.0f\n',it2*2*s);
    fprintf('Rank of X is                        %8.0f\n',rk2);
    fprintf('Computed relative residual norm  is %3.2e\n',rr2);
    fprintf('CPU time is                         %3.2e\n',t2);
    fprintf('\n')
    if (n < s_max)
        X2 = V2*Y2*V2'; X22 = Z2*Z2';
        res2 = norm(A'*X2+X2*A-X2*B*B'*X2+C'*C,'fro');
        res22 = norm(A'*X22+X22*A-X22*B*B'*X22+C'*C,'fro');
        fprintf('Exact residual norm with X = Z*Z^T is     %3.2e\n',res2);
        fprintf('Exact residual norm with X = V*Y*V^T is   %3.2e\n',res22);
    end
    fprintf('\n')
end